image:
  - /assets/img/products/specialty-gas-regulators.jpg
overview: |
  Specialty pressure regulators, diaphragm valves, and filters are used by the analytical, instrumentation, petrochemical, oil & gas, and specialty gas industries.   Regulators and valves are designed to fulfill a wide range of applications, controlling pressure while accommodating low to moderate flow rates of many gases and liquids.
  
  The line includes single stage, back pressure, dome loaded, high purity, liquid and gas pressure regulators, and filters for process analyzers.
  
sub-product:
  -
    type: product
    title: 'Pressure Regulator-Back Pressure'
    brand_manufacturer: 59e480ed-caf1-481f-9771-837d5833b697
    link_to_product: 'https://goreg.com/product-category/pressure-regulators/back-pressure/'
    image:
      - /assets/img/products/pressure-regulator-back-pressure.jpg
  -
    type: product
    title: 'Pressure Regulator-Cylinder'
    brand_manufacturer: 59e480ed-caf1-481f-9771-837d5833b697
    link_to_product: 'https://goreg.com/product-category/pressure-regulators/cylinder/'
    image:
      - /assets/img/products/pressure-regulator-cylinder.jpg
  -
    type: product
    title: 'Pressure Regulator-Dome Loaded'
    brand_manufacturer: 59e480ed-caf1-481f-9771-837d5833b697
    link_to_product: 'https://goreg.com/product-category/pressure-regulators/dome-loaded/'
    image:
      - /assets/img/products/pressure-regulator-dome-loaded.jpg
  -
    type: product
    title: 'Pressure Regulator-High Pressure'
    brand_manufacturer: 59e480ed-caf1-481f-9771-837d5833b697
    link_to_product: 'https://goreg.com/product-category/pressure-regulators/high-pressure/'
    image:
      - /assets/img/products/pressure-regulator-high-pressure.jpg
  -
    type: product
    title: 'Pressure Regulator-Single Stage'
    brand_manufacturer: 59e480ed-caf1-481f-9771-837d5833b697
    link_to_product: 'https://goreg.com/product-category/pressure-regulators/single-stage/'
    image:
      - /assets/img/products/pressure-regulator-single-stage.jpg
  -
    type: product
    title: 'Pressure Regulator-Vaporizing'
    brand_manufacturer: 59e480ed-caf1-481f-9771-837d5833b697
    link_to_product: 'https://goreg.com/product-category/pressure-regulators/vaporizing/'
    image:
      - /assets/img/products/pressure-regulator-vaporizing.jpg
title: 'Specialty Gas Regulators'
categories:
  - regulators
tags:
  - cranego
id: 4818cf14-1c25-42a6-a2fc-05a6b3d49237
