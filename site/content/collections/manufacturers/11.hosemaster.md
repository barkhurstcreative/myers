title: 'Hose Master'
brand_website: 'https://www.hosemaster.com/'
short_tag: hosemaster
image:
  - /assets/img/logos/hose-master-logo.jpg
short_description: 'Hose Master is North America’s largest manufacturer of flexible metal hose and metal bellows expansion joints. Hose Master has grown through product innovation and application expertise to define the industry standard.'
id: 87b66bd1-2c12-4dac-959c-daf437ad05c2
