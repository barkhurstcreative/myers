title: Resources
meta_title: 'Product guides and resources for specialty distribution || Myers and Co'
meta_description: 'Servicing Pennsylvania, Ohio, West Virginia, and Maryland. Explore product guide catalogs and resources.'
template: resources
fieldset: default
id: a5dc98fb-acf9-4351-a2eb-3fe8d60d814a
