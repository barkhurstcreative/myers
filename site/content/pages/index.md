---
heading: 'Committed to Quality and Integrity'
description: 'For almost 30 years, we''ve met each customer’s needs by providing quality products in a timely and professional manner.'
call_out_description: 'Serving a diverse customer base throughout Pennsylvania, Ohio, West Virginia and Maryland, Myers''s markets include chemical, steel, natural gas, hydraulic and analytical, and our products are in demand by contractors, OEMs and those working in R&D.'
meta_title: 'Regional Specialty Distributor • Myers and Co'
meta_description: 'Serving a diverse customer base throughout Pennsylvania, Ohio, West Virginia and Maryland, Myers''s markets include chemical, steel, natural gas, hydraulic and analytical, and our products are in demand by contractors, OEMs and those working in R&D.'
posts: 3
title: Home
template: home
fieldset: home
id: db0ae4e3-4f10-4802-bc40-0b880cbf02c7
---
At Myers our mission is to provide high-quality, cost-effective specialty products including instrumentation, process valves, tube fittings, hose assemblies and regulators along with value-added services in a timely manner to clients with industrial applications in Pennsylvania, Ohio, West Virginia and Maryland.