title: About
about: About
about_description: |
  <p><strong>In 1993, Myers & Co. was founded </strong>in Mars, PA as a family-based, customer-focused specialty distributor of instrumentation, tube fittings, process instrument valves and manifolds, flanged & 3-piece process ball valves, pressure-reducing regulators, pressure gauges and transmitters, temperature indicators and transmitters, flexible metal hose assemblies, sample cylinders and custom fabrications. The original founders started Myers based on a passion for serving the needs of our customers, and that core principle continues on today.
  </p>
history_description: |
  <p>Myers has more than 80 years of combined experience serving the chemical, steel, natural gas, hydraulic and analytical markets. We also provide parts and services to OEMs, contractors, and R&D projects.
  </p>
  <p><u></u>
  </p>
  <p>At Myers we recognize that being a supplier is a privilege. We strive to maintain the respect and trust of our customers with integrity and the constant commitment to “doing the right thing.” We also aim to be flexible in all situations regarding product offering, pricing, delivery, and service.<u></u><u></u>
  </p>
  <p><span class="gmail-m_-1770853909844258163redactor-invisible-space"><strong>Just as the company was founded </strong>with a commitment to customer service, our primary goal today is to meet each customer’s need by providing quality products in a timely and professional manner. We also understand that we have our customers and suppliers to thank for our past success, and we know that our continued growth will only be sustained by meeting and exceeding customer expectations.</span>
  </p>
meta_title: 'For chemical, steel, natural gas, hydraulic and analytical markets || Myers and Co'
meta_description: 'Specialty distributor serving the chemical, steel, natural gas, hydraulic and analytical markets since 1993.'
fieldset: about
template: about
id: 99d397a5-fa78-4775-b488-1b07ccad51a1
